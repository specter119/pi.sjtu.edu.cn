+++
date = "2013-12-06T22:36:58+08:00"
draft = false
title = "Design a new site for SJTU Pi Supercomputer"
+++

首页布局
======

集群概况
------

介绍集群概要、集群照片和服务对象。

服务内容
------

介绍对外提供服务的几个集群、服务政策、服务等级、定价以及如何申请资源。

使用HPC服务
------

介绍如何使用HPC服务，包括：连接集群、编译代码、运行作业等。

寻求帮助
------

介绍如何请求技术支持，以及集群使用的常见问题。

其他内容
------

办公室地址、集群整体运行状态、集群整体状态、与工业界的合作、HPC排名、Logo、搜索框、版权声明、联系我们、友情链接、内容索引A-Z。


*HPCS和 Centre for Scientific Computing of Cambridge University 的联系?*

服务内容 Services & policies
======

Darwin集群介绍
------
 
照片、历史概况、节点数、处理器架构和硬件配置。

Wikes集群介绍
------

照片、历史概况、节点数、处理器架构和硬件配置。
友情链接：相关新闻、The Green 500。

政策 Usage polices
------

机时计费、服务等级(正常付费用户、临时使用、小规模免费使用)、存储配额。
集群可用性和定期维护。
其他补充条款。

申请机时
------

申请表格和电子邮件地址。

用户指南 Working on the HPCS
======

快速入门
------

### 登录

使用统一登录服务器，后台做负载均衡：

	ssh <username>@login.hpc.cam.ac.uk

介绍集群使用的硬件，主要是CPU型号，以及推荐的CPU优化参数```-xSSE4.2```、```-axAVX```等。

### 密码

如何更改密码，以及对密码强度的建议。

### 文件系统

文件系统简介。

### Modules

介绍Modules和常用的Modules操作命令，常见模块如下：

	[sjr20@login-sand1 ~]$ module list
	Currently Loaded Modulefiles:
	  1) dot                     5) java/jdk1.7.0_07        9) global	13) default-impi
	  2) torque                  6) turbovnc/1.1           10) intel/cce/12.1.10.319
	  3) scheduler               7) vgl/2.3.1/64           11) intel/fce/12.1.10.319
	  4) gold/2.2.0.5            8) intel/impi/4.0.3.008   12) intel/mkl/10.3.10.319

### ```.bashrc```

给```.bashrc```的修改提了建议。

### 编译环境

一点点编译建议，如何使用mpicc、使用优化参数等。
如果有缺失的库，可以和管理员联系，这样管理员可以集中安装。

### 并行作业提交和运行

对于运行机时的限制：xxx核、总共运行多长时间(walltime)。
对服务等级(Quality of Service)有比较清楚的定义，对用户类型也作业区分。
提供了一些MPI运行的例子examples：sandybridge, westmere, tesla。
罗列最简单的几个```qsub```命令。

### 问题反馈和寻求帮助

如果用户检查FAQ以后问题没有解决，建议联系客服，给出了一些比较规范的客服联系方法。

连接HPC集群
------

### SSH连接

SSH前端使用了一台负载均衡服务器。
可以加上```-X```或者```-Y```参数进行X forward。
在X forward中，不需要使用```xauth```或者```xhost```这种不安全的命令。

### 使用密码的建议 

密码长度、数字与字母组合、不使用字典词等。

### VNC远程桌面

用户可以使用VNC连接到远程桌面，请参考"Remote desktop & 3D"一节。

### 文件传输

支持SSH隧道的数据传输：scp, sftp, rsync。
Windows用户可以使用WinSCP或者Cyberduck客户端。 
rsync数据同步传输最快(差分同步)，但是也最危险，需要小心使用。

### 其他登录节点的信息

登录节点仅仅用于：编译代码、程序调试、提交作业、监控程序运行、后续数据处理。
罗列后端登录节点完整列表。

用户环境和modules
------

登录系统时会自动加载一些 environment modules，用户可以编辑自己的```.bashrc```来定制自动加载的列表，提醒用户在编辑环境变量的时候不要把原来的值覆盖了。

自动加载的模块列表，命名方式可作参考：

	Currently Loaded Modulefiles:
	  1) dot                     5) java/jdk1.7.0_07        9) global                 13) default-impi
	  2) torque                  6) turbovnc/1.1           10) intel/cce/12.1.10.319
	  3) scheduler               7) vgl/2.3.1/64           11) intel/fce/12.1.10.319
	  4) gold/2.2.0.5            8) intel/impi/4.0.3.008   12) intel/mkl/10.3.10.319

几个常用的module命令：```avail```, ```whatis```, ```load```, ```unload```, ```purge```。

用户还可以编写自己的module。

文件系统
------

介绍几个文件系统的用途、容量、配额、备份策略、删除策略等。

编译代码
------

介绍集群中可用的编译器、建议的编译器优化参数、MPI对编译器后端的封装。给了一个MPI程序的例子，并对函数功能做了说明。

运行作业
------

PBS系统简介。分别给出非MPI和MPI作业提交的例子。
作业提交```qsub```命令常用参数、作业运行状态监控。
Maui资源管理系统命令，用于真正的作业管理。
批量作业、短作业。

交互运行
------

可以在登录节点上启动一些小规模的测试作业。
作业启动后，用户可以登录到节点查看作业运行情况。
PBS本身也提供了一定的交互运行能力。
3D可视化可参考"Remote desktops & 3D visualization for details"一节。

远程桌面与数据可视化
------

传统的交互方式包括命令行交互，以及使用SSH Forward的X11图形界面交互。
HPCS还提供基于VNC的远程桌面连接，可进行完整的2D和3D图形(部分加速)用户界面交互。
介绍如何在服务器端启动VNC进程、使用SSH透传VNC、客户端VNC连接、VNC 3D加速。

配备了一些3D可视化的软件：IDL、ParaView、PyMOL、Partiview CVS、VisIt、VMD等。
其中ParaView是优秀的开源3D可视化软件，获得了HPCwire 2012编辑选择奖。

电子邮件通知
------

介绍如何在命令行收发邮件、通过邮件接收作业运行通知。

软件列表
------

建议用module加载软件，而不是到文件系统下找已经安装的软件。
准备了一些MPI作业提交的模板供用户使用。
对软件的授权做了说明，部分软件仅限学术用途，部分软件需要用户自己提供License。

寻求帮助 Getting help
======

### 如何请求支持

包括高性能计算支持组的联系方式(电话和邮箱)，以及几种错误类型和请求支持时需要提供的信息。
 
* 编译错误：想要做什么操作、```module list```输出、编译目录、运行的命令、错误信息输出。
* 作业运行错误：先使用```qstat -a```和```checkjob -v <jobid>```进行自查，并留意作业输出文件的内容。
如果这些自查不能解决问题，请提供如下信息：
jobid、作业内容简要描述、作业提交脚本、用于触发作业脚本的命令、在作业输出文件中出现的错误信息、从电子邮件中收到的错误信息。
* 请求安装应用和函数库：应用和函数库的名字、主页、为什么需要安装这个库、库的授权形式、其他具体要求(32bit/64bit、单精度/双精度等)。

### FAQ

FAQ内容分为这几个部分介绍：接入HPC集群(Access)、运行作业(Running Jobs)、付费(Credits, quarters and paid usage)、远程桌面连接(Remote Desktops)、其他一般问题(General)。

#### 接入HPC集群

* 如何查看自己网络的公网IP地址，我可以从什么网络连接HPC集群。
* 服务器的SSH指纹。
* 如何更改密码。
* 使用跳板节点从别的地方接入HPC集群。给用户提醒，注册动态或者网关IP是不安全的行为，从跳板节点登录也是不安全的。

#### 运行作业

* 在登录节点直接运行作业的资源约束；
* PBS(Torque+Maui)作业调度系统简介；
* 提交作业时，如何选择合适的核数、节点数、内存大小，如何处理MPI/OpenMP混合作业？
* 作业启动与作业等待时间，设置作业优先级、使用```showstart```估算作业启动时间；
* 一次向作业系统提交多个同类型(资源需求相同)的作业。
* 有另一个用户提交了大量的单核串行作业，这对其他用户的作业是否有影响？(答案是否定的，这是好事，调度系统不是一个简单的先进先服务系统，而是一个公平调度系统)
* 如何让多核作业在一个计算节点上运行；
* 如何让一个作业独占一个计算节点(Cambridge目前默认的配置是一个作业会独占一个节点，用户需要为整个节点的计算核心付费。这一点我觉得我们的配置更加灵活，用户可以选择作业是否独占某个计算节点。)
* 如何提高stack size大小？(在```limits```中设置)
* 为什么cron作业失效了?
* Fortran MPI作业异常终止，提示```cannot overwrite existing file errors```。
* 使用MKL时提示```libmkl.so: file too short``` 。
* 新的MKL链接时使用的flags。
* 如何解决```InfiniPath...not```问题？

#### 付费

* 计费系统概述(Gold Allocation Manager)。也讨论了为什么要收费、财政季度的问题、资源分配、免费资源使用、服务等级、大机时合作等问题。
* 如何购买机时；

#### 远程桌面连接

* 收到提示```Authentication is required...```怎么办？

#### 其他问题

* 如在科研工作中向HPCS致谢？
* ```Nehalem```、```Westmere```、```Sandy Bridge```这些节点是怎么回事，如何添加恰当的编译优化参数。
* ```/scratch```目录怎么不见了？(因为使用了autofs的缘故呀)

参考资料
======
* "High Performance Computing Service in Cambridge University" <http://www.hpc.cam.ac.uk/user/quickstart.html>
* "UNIX Tutorial for Beginners" <http://www.ee.surrey.ac.uk/Teaching/Unix/>
* "LSF Documentation" <http://bmi.cchmc.org/resources/clusters/lsf-migration-1/lsf-documentation>
