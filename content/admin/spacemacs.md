---
date: 2016-12-10T13:49:58+08:00
title: Spacemacs for Vim users -- basics and some of my favorite layers
---

Spacemacs Installation
======

| Key                                                                                                                | Operation                               |
|--------------------------------------------------------------------------------------------------------------------|-----------------------------------------|
| `cd ~; mv .emacs.d .emacs.d.bak; mv .emacs .emacs.bak; git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d` | Install Spacemacs.                      |
| `cd ~/.emacs/; git fetch; git reset --hard <tag>`                                                                  | Update Spacemacs to a specific version. |

Spacemacs Basics
======

Help
------

| Key         | Operation                      |
|-------------|--------------------------------|
| `SPC h SPC` | Access documentation           |

`.spacemacs` configuration file
------

| Key         | Operation                      |
|-------------|--------------------------------|
| `SPC f e d` | Edit the configuration file.   |
| `SPC f e R` | Reload the configuration file. |
| `SPC q r`   | Restart spacemacs.             |

File operation
------

| Key       | Operation                            |
|-----------|--------------------------------------|
| `SPC f f` | Fuzzy search in Spacemacs.           |
| `SPC f r` | Open a list of recent files.         |
| `SPC f s` | Save a file in Spacemacs.            |
| `SPC f j` | Open the current directory in dired. |

Buffer
------

| Key       | Operation                            |
|-----------|--------------------------------------|
| `SPC b b` | List buffers and switch between them.|
| `SPC TAB` | Switch to the last buffer.           |
| `SPC b D` | Kill this buffers.                   |
| `SPC b m` | Kill all other buffers.              |

Window
------

| Key         | Operation                            |
|-------------|--------------------------------------|
| `SPC w d`   | Close this window                    |
| `SPC w s/v` | Split windows                        |

`NeoTree` layer
------

| Key         | Operation                                        |
|-------------|--------------------------------------------------|
| `SPC f t`   | Toggle the NeoTree                               |
| `SPC 0`     | Jump to the NeoTree window                       |
| `h`         | collapse expanded directory or go to parent node |
| `K`         | parent directory                                 |
| `R`         | make a directory as the root directory           |
| `|`         | open a file in a vertically split window         |
| `-`         | open a file in a horizontally split window       |
| `TAB`       | toggle stretching of the buffer                  |
| `c`         | create a node                                    |
| `d`         | delete a node                                    |
| `r`         | rename a node                                    |
| `g`         | refresh                                          |
| `s`         | toggle showing the hidden files                  |

`Ranger` layer
------

| Key Binding | Description                                          |
|-------------|------------------------------------------------------|
| `SPC a r`   | launch ranger                                        |
| `q`         | quit                                                 |
| `S`         | Enter shell                                          |
| `m`         | Mark                                                 |
| `u`         | Unmark                                               | 
| `C`         | Copy                                                 | 
| `R`         | Rename/Remove                                        | 
| `D`         | Delete                                               | 
| `g`         | Refresh                                              | 
| `x`         | Execute all operations                               | 

`shell` layer
------

| Key             | Operation                              |
|-----------------|----------------------------------------|
| `SPC '`         | open, close or go to the default shell |
| `SPC a s m`     | open, close or go to `multi-term`      |

In `multi-term` mode:

| Key             | Operation                     |
|-----------------|-------------------------------|
| `SPC m c`       | create a new multi-term       |
| `SPC m n`       | go to the next multi-term     |
| `SPC m p`       | go to the previous multi-term |

`latex` layer
======

| Key             | Operation                |
|-----------------|--------------------------|
| `SPC m e`       | Insert LaTeX environment |
| `SPC m i`       | Insert LaTeX `\item`     |
| `SPC m r c`     | `reftex-citation`        |
| `SPC m r c`     | `reftex-citation`        |

`markdown` layer
================

| Key                                   | Operation                                     |
|---------------------------------------|-----------------------------------------------|
| `SPC m i l`                           | inser a link                                  |
| `SPC m i L`                           | inser a link dwim                             |
| `SPC m i u`                           | inser a URL                                   |
| `SPC m i i`                           | inser an image                                |
| `SPC m x b`                           | make a region bold or insert bold             |
| `SPC m x i`                           | make a region italic or insert italic         |
| `SPC m x c`                           | make a region code or insert code             |
| `SPC m x q`                           | make a region blockquote or insert blockquote |
| `SPC m x p`                           | make a region `<pre>` or insert `<pre>`       |
| `SPC m x >`                           | indent a region                               |
| `SPC m x <`                           | extend a region                               |
| `SPC : markdown-toc/generate-toc RET` | generate a table of contents                  |

`lisp` layer
======

| Key             | Operation                    |
|-----------------|------------------------------|
| `SPC m e e`     | Evaluate the last expression |

Spell checking
======

| Key        | Operation                 |
|------------|---------------------------|
| `SPC S b`  | flyspell the whole buffer |
| `SPC S c`  | flyspell correct          |
| `SPC S d`  | flyspell dictionary       |
| `SPC S n`  | flyspell goto next error  |
| `SPC t S`  | toggle flyspell           |

Spell checking
======

| Key        | Operation                 |
|------------|---------------------------|
| `SPC e c`  | clear errors              |
| `SPC e l`  | display a list errors     |
| `SPC e V`  | verify flyspell setup     |
| `SPC t s`  | toggle flycheck           |


Reference
======
* "Spaceamcs" <https://github.com/syl20bnr/spacemacs>
* "Shell contribution layer for Spacemacs" <https://github.com/syl20bnr/spacemacs/tree/master/layers/shell>
* "LaTeX Layer for Spacemacs" <https://github.com/syl20bnr/spacemacs/tree/master/layers/%2Blang/latex>
* "Markdown contribution layer for Spacemacs" <https://github.com/syl20bnr/spacemacs/tree/master/layers/%2Blang/markdown> 
