---
date: 2018-04-18T16:44:05+08:00
draft: false
title: Installing and running HiC-Pro
type: post
---

Installing HiC-Pro
======

Installing dependency packages
------

### bowtie2 

```
$ spack install bowtie2 %gcc@5.4.0
```

### samtools

```
$ spack install samtools %gcc@5.4.0
```

### Python 2.7 with numpy, scipy, pysam and bx-python

```
$ spack install py-numpy+blas+lapack %gcc@5.4.0 ^openmp threads=openmp
$ spack install py-scipy %gcc@5.4.0 ^openblas threads=openmp
$ spack install py-pysam %gcc@5.4.0
$ spack spec py-bx-python %gcc@5.4.0 ^py-numpy+blas+lapack^openblas threads=openmp
```

### R with RColorBrewer and Ggplot2 packages

Follow instructions on https://pi.sjtu.edu.cn/doc/R .

```
$ spack install r-rcolorbrewer %gcc@5.4.0 ^r+external-lapack^openblas threads=openmp
$ spack install r-ggplot2 %gcc@5.4.0 ^r+external-lapack^openblas threads=openmp
```

Building HiC-Pro
------

Reference
======
* https://github.com/nservant/HiC-Pro
