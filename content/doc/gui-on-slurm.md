---
date: 2018-03-25T12:39:27+08:00
title: Running GUI Apps on SLURM
type: post
---

1. Login to Pi via [X2GO](https://pi.sjtu.edu.cn/doc/x2go).

2. Start `Terminal.app` in your X2GO desktop environment. 

3. In Termial.app, request an exclusive node to run X Windows job and check which compute node is granted.

	$ salloc -p cpu -N 1 --exclusive; squeue -u `whoami` --state=running
    
4. Login to the compute node, say `node300`, with X-forwarding enable.

    $ ssh -Y node300
    
5. Start your GUI app on the compute node, whose GUI should be displayed within your X2GO environment.

	$ ./START_YOUR_GUI_APP 
    
6. When everything completes, log out from X2GO via `System` -> `Logout`.
