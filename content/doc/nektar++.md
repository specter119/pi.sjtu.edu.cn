+++
title = "Nektar++"
date =  2019-04-12T15:43:58+08:00
+++

Installing Spack
======

	$ cd
	$ git clone https://github.com/sjtuhpcc/spack.git
    $ cd spack
    
Add the following settings to ```~/.bashrc``` or ```~/.bash_profile```.

	# Spack package management
    if [ -d "$HOME/spack" ]; then
        export SPACK_ROOT=$HOME/spack
        source $SPACK_ROOT/share/spack/setup-env.sh
    fi	

Using Nektar++
======

Install nektar++:

    $ spack install nektar %gcc@5.4.0 ^openmpi@2.0.4+pmi fabrics=verbs schedulers=slurm ^cmake@3.9.6

The following job (```cpu.slurm```) requests all 16 CPU cores per node, which can be submitted via ```sbatch -N 1 cpu.slurm```.

    #!/bin/bash

    #SBATCH -J nektar
    #SBATCH -p cpu
    #SBATCH --mail-type=end
    #SBATCH --mail-user=YOU@EMAIL.COM
    #SBATCH -o %j.out
    #SBATCH -e %j.err
    #SBATCH -N1
    #SBATCH --exclusive

    ulimit -s unlimited
    ulimit -l unlimited

    module purge
    source <(spack module tcl loads --dependencies nektar %gcc@5.4.0 )

    srun --mpi=pmi2 IncNavierStokesSolver-rg