+++
date = "2018-10-09T10:43:41+08:00"
draft = false
title = "VMD"
+++

Install pre-compiled Version of VMD
======

Download pre-comiled text-mode vmd.

	$ cd $HOME/tmp
    $ wget http://spack.pi.sjtu.edu.cn/mirror/vmd/vmd-1.9.3.bin.LINUXAMD64.text.tar.gz

Extract the tar.

	$ cd $HOME/tmp
	$ tar xzvpf vmd*.tar.gz

Configure installation directories.

    $ cd $HOME/tmp
	$ mkdir -p $HOME/vmd-1.9.3-textmode/bin
    $ export VMDINSTALLBINDIR="$HOME/vmd-1.9.3-textmode/bin" 
	$ mkdir -p $HOME/vmd-1.9.3-textmode/lib
    $ export VMDINSTALLLIBRARYDIR="$HOME/vmd-1.9.3-textmode/lib" 
    $ cd vmd-1.9.3
    $ ./configure

Install vmd.

	$ cd ~/tmp/vmd-*/src/
    $ make install

Check if vmd has been installed sucessfully.

	$ $HOME/vmd-1.9.3-textmode/bin/vmd -h
    
Submit the following job script `vmd.slurm` to SLURM job scheduling system via `sbatch -p cpu vmd.slurm`.

```
#!/bin/bash

#SBATCH --job-name=vmd
#SBATCH --partition=cpu
#SBATCH --mail-type=end
#SBATCH --mail-user=YOU@EMAIL.COM
#SBATCH --output=%j.out
#SBATCH --error=%j.err
#SBATCH -p cpu
#SBATCH -n 1
#SBATCH --exclusive

ulimit -l unlimited
ulimit -s unlimited

$HOME/vmd-1.9.3-textmode/bin/vmd YOUR VMD OPTIONS
```
   
Reference
======
* "Installing a Pre-Compiled Version of VMD" <https://www.ks.uiuc.edu/Research/vmd/current/ig/node6.html>
