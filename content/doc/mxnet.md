---
date: 2018-04-01T16:44:05+08:00
draft: false
title: Running MXNet
type: post
---

Running MXNet on CPU
======

Install pre-built MXNet from Anaconda.

    $ module load miniconda3/4.5
    $ conda config --add channels https://mirrors.sjtug.sjtu.edu.cn/anaconda/pkgs/free/
    $ conda config --add channels https://mirrors.sjtug.sjtu.edu.cn/anaconda/pkgs/main/
    $ conda config --set show_channel_urls yes
    $ conda create --name mxnet-py3-cpu numpy ipython jupyter matplotlib graphviz
    $ source activate mxnet-py3-cpu
    $ pip install mxnet

Verify the installation by loading MXNet module.**DO NOT run MXNet jobs on login nodes.**

	$ python -c 'import mxnet as mx; print(mx.__version__)'
    1.1.0

Log out and log in again, then request an interactive SLURM job to run MXNet.

	$ module purge
	$ srun -p k80 -N 1 --exclusive --pty /bin/bash
    hostname
    module load miniconda3/4.5
    source activate mxnet-py3-cpu
	python -c 'import mxnet as mx; print(mx.__version__)'

Prepare a job script `mxnetcpu.slurm` with contents as follows.

	#!/bin/bash
	
	#SBATCH -J mxnet-cpu
	#SBATCH -p k80
	#SBATCH --mail-type=end
	#SBATCH --mail-user=YOU@EMAIL.COM
	#SBATCH -o %j.out
	#SBATCH -e %j.err
	#SBATCH -n 1
	#SBATCH --exclusive
	
	source /usr/share/Modules/init/bash
	module purge
	module load miniconda3/4.5
	
	source activate mxnet-py3-cpu
	python -c 'import mxnet as mx; print(mx.__version__)'

Then submit this job to SLURM. Please refer to <https://pi.sjtu.edu.cn/doc/slurm> for SLURM usage.

	$ sbatch mxnetcpu.slurm

Running MXNet on GPU
======

Install pre-built GPU MXNet from Anaconda.

    $ module load miniconda3/4.5
    $ conda create --name mxnet04-py3-gpu numpy ipython jupyter
    $ source activate mxnet-py3-gpu
    $ pip install mxnet-cu90mkl

<!--
Verify the installation by loading MXNet module on `mu07(202.120.58.231)`.**DO NOT run TenorFlow jobs on login nodes.**

    $ module purge; module load miniconda3/4.5 cuda/9.0 cudnn/7.0
    $ source activate mxnet04-py3-gpu
	$ python -c 'import mxnet as mx; print(mx.__version__)'
    1.1.0
-->

Log out and log again, then request an interactive SLURM job to run MXNet.

	$ module purge
	$ srun -p k80 -N 1 --exclusive --gres=gpu:1 --pty /bin/bash
    hostname
    module load miniconda3/4.5 cuda/9.0 cudnn/7.0
    source activate mxnet-py3-gpu
	python -c 'import mxnet as mx; print(mx.__version__)'
    1.1.0

Prepare a job script `mxnetgpu.slurm` with contents as follows.

	#!/bin/bash
	
	#SBATCH -J mxnet-gpu
	#SBATCH -p k80
	#SBATCH --mail-type=end
	#SBATCH --mail-user=YOU@EMAIL.COM
	#SBATCH -o %j.out
	#SBATCH -e %j.err
	#SBATCH -N 1
	#SBATCH --exclusive
	
	source /usr/share/Modules/init/bash
	module purge
	module load miniconda3/4.5 cuda/9.0 cudnn/7.0
	
	source activate mxnet-py3-gpu
	python -c 'import mxnet as mx; print(mx.__version__)'

Then submit this job to SLURM. Please refer to <https://pi.sjtu.edu.cn/doc/slurm> for SLURM usage.

	$ sbatch mxnetgpu.slurm

Reference
======
* http://gluon.ai
