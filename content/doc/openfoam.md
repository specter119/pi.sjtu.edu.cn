+++
date = "2018-06-03T16:43:41+08:00"
draft = false
title = "OpenFOAM"
+++

Install Spack
======

	$ cd
	$ git clone https://github.com/sjtuhpcc/spack.git
    $ cd spack
    $ ./bootstrap.sh user --install
    
Add the following settings to ```~/.bashrc``` or ```~/.bash_profile```.

	# Spack package management
    if [ -d "$HOME/spack" ]; then
        export SPACK_ROOT=$HOME/spack
        source $SPACK_ROOT/share/spack/setup-env.sh
    fi	

Build OpenFOAM 4.1 via Spack
======

	$ spack install openfoam-org@4.1 %gcc@5.4.0 ^openmpi@2.0.4+pmi fabrics=verbs schedulers=slurm ^flex@2.6.4 ^cmake@3.9.6

Here is a SLURM job sample for OpenFOAM 4.1

```
#!/bin/bash

#SBATCH -J openfoam
#SBATCH -p cpu
#SBATCH --mail-type=ALL
#SBATCH --mail-user=REPLACE.WITH.YOUR@EMAIL.ADDRESS
#SBATCH -e %J.err
#SBATCH -o %J.out
#SBATCH --ntasks-per-node=16

source /usr/share/Modules/init/bash

# Spack package management
if [ -d "$HOME/spack" ]; then
    export SPACK_ROOT=$HOME/spack
    source $SPACK_ROOT/share/spack/setup-env.sh
fi

source <( spack module tcl loads --dependencies openfoam-org@4.1 %gcc@5.4.0 )
source <( spack location -i openfoam-org@4.1 %gcc@5.4.0 )/etc/bashrc

srun --mpi=pmi2 icoFoam -parallel
```
Build OpenFOAM 5.0 via Spack
======

	$ spack install openfoam-org@5.0 %gcc@5.4.0 ^openmpi@2.0.4+pmi fabrics=verbs schedulers=slurm ^flex@2.6.4 ^cmake@3.9.6

Here is a SLURM job sample for OpenFOAM 5.0

Usage: `sbatch job.slurm`

```
#!/bin/bash

#SBATCH -J openfoam
#SBATCH -p cpu
#SBATCH --mail-type=ALL
#SBATCH --mail-user=REPLACE.WITH.YOUR@EMAIL.ADDRESS
#SBATCH -e %J.err
#SBATCH -o %J.out
#SBATCH --ntasks-per-node=16

source /usr/share/Modules/init/bash

# Spack package management
if [ -d "$HOME/spack" ]; then
    export SPACK_ROOT=$HOME/spack
    source $SPACK_ROOT/share/spack/setup-env.sh
fi

source <( spack module tcl loads --dependencies openfoam-org@5.0 %gcc@5.4.0 )
source <( spack location -i openfoam-org@5.0 %gcc@5.4.0 )/etc/bashrc

srun --mpi=pmi2 icoFoam -parallel
```

Build OpenFOAM-com v1612 via Spack
======

	$ spack install openfoam-com+metis@1612 %gcc@5.4.0 ^openmpi@2.0.4+pmi fabrics=verbs schedulers=slurm ^flex@2.6.4 ^cmake@3.9.6

Here is a SLURM job sample for OpenFOAM-com v1612

Usage: `sbatch job.slurm`

```
#!/bin/bash

#SBATCH -J openfoam
#SBATCH -p cpu
#SBATCH --mail-type=ALL
#SBATCH --mail-user=REPLACE.WITH.YOUR@EMAIL.ADDRESS
#SBATCH -e %J.err
#SBATCH -o %J.out
#SBATCH --ntasks-per-node=16

source /usr/share/Modules/init/bash

# Spack package management
if [ -d "$HOME/spack" ]; then
    export SPACK_ROOT=$HOME/spack
    source $SPACK_ROOT/share/spack/setup-env.sh
fi

source <( spack module tcl loads --dependencies openfoam-com@1612 %gcc@5.4.0 )
source <( spack location -i openfoam-com@1612 %gcc@5.4.0 )/etc/bashrc

srun --mpi=pmi2 icoFoam -parallel
```
