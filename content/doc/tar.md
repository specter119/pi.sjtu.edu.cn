+++
date = "2014-10-30T18:37:34+08:00"
draft = false
title = "Tar tutorial"
+++

This tutorial aims to highlight some useful features of the tar command line.
We strongly recommend to read the output of the command

	man tar

to understand the meaning of the options use in this tutorial.

The powerful tar command can make  your daily work on Pi easier, here are three way of how to use it:

How to extract and make an archive
======

To simply untar a .tar archive type the command

	tar xvf tarball.tar

Most of tar files we have to deal with are compressed either with gzip, bzip2 or xz.

Here is how to proceed for each cases:

	tar xvzf tarball.tar.gz ( or tarball.tgz )
	tar zxjf tarball.tar.bzip2
	tar xvJf tarball.tar.xz ( or tarball.txz ) 

In order to compress a folder we have to use tar first. 

Here is how to proceed in the case of archiving two folders:

	tar cvf mytarball.tar dir1 dir2

We can then compress the resulting mytarball.tar.

it's also possible to archive and compress in one tar command:

	tar cvzf mytarball.tar.gz dir1 dir2
	tar cvjf mytarball.tar.bzip2 dir1 dir2
	tar cvJf mytarball.tar.xz dir1 dir2

How to list a tarball content
======

An archive can get very big, one can know it's content without extract it.

To list files of an archive just do:

	tar tvf tarball.tar

In the case of a gzip archive one do

	tar tvzf tarball.tgz

Extracting specific files
======

To extract a specific file from a tarball just execute

	tar -xvf tarball.tar path/to/file

where path/to/file was obtained from the content listing (see above).
