+++
date = "2018-01-16T16:43:41+08:00"
draft = false
title = "Python"
weight = 60
+++

This document shows you how to use Miniconda to up customized Python environment in your `$HOME` directory.
Depending on your preferred Python version, 2 or 3, two Miniconda are available.

1. [Miniconda 2](#anaconda2)
2. [Miniconda 3](#anaconda3)

<a id="anaconda2"></a>Miniconda 2
======

Load Miniconda version 2.

```bash
$ module purge
$ module load miniconda2/4.5
```

Create a conda envirionment along with selected Python packages.

```bash
$ conda config --add channels https://mirrors.sjtug.sjtu.edu.cn/anaconda/pkgs/free/
$ conda config --add channels https://mirrors.sjtug.sjtu.edu.cn/anaconda/pkgs/main/
$ conda config --set show_channel_urls yes
$ conda create --name mypython2 numpy scipy matplotlib ipython jupyter
```

Activate your python environment.

```bash
$ source activate mypython2
```

Add more packages via `conda` or `pip`.

```bash
$ conda install YOUR_PACKAGE
$ pip install YOUR_PACKAGE
```

<a id="anaconda2"></a>Miniconda 3
======

Load Miniconda version 3.

```bash
$ module purge
$ module load miniconda3/4.5
```

Create a conda envirionment along with selected Python packages.

```bash
$ conda config --add channels https://mirrors.sjtug.sjtu.edu.cn/anaconda/pkgs/free/
$ conda config --add channels https://mirrors.sjtug.sjtu.edu.cn/anaconda/pkgs/main/
$ conda config --set show_channel_urls yes
$ conda create --name mypython3 numpy scipy matplotlib ipython jupyter
```

Activate your python environment.

```bash
$ source activate mypython3
```

Add more packages via `conda` or `pip`.

```bash
$ conda install YOUR_PACKAGE
$ pip install YOUR_PACKAGE
```
