---
title: "Vasp"
date: 2019-02-28T11:15:50+08:00
draft: false
---

Compile VASP
=============

1. decompress VASP

        $ tar xvf vasp.5.4.4.tar.bz2
        $ cd vasp.5.4.4
    
2. setting up VTST

        $ wget http://theory.cm.utexas.edu/code/vtstcode.tgz
        $ tar xvf vtstcode.tgz
    
3. backup some files in VASP(optional)

        $ cp src/chain.F src/chain.F-org

4. replace some files from VTST

        $ cp vtstcode-171/* src/
    
5. modify some source code

    in src/main.F line 3146
    turn

        CALL CHAIN_FORCE(T_INFO%NIONS,DYN%POSION,TOTEN,TIFOR, &
             LATT_CUR%A,LATT_CUR%B,IO%IU6)
             
    into

        CALL CHAIN_FORCE(T_INFO%NIONS,DYN%POSION,TOTEN,TIFOR, &
             TSIF,LATT_CUR%A,LATT_CUR%B,IO%IU6)
        !     LATT_CUR%A,LATT_CUR%B,IO%IU6)

    in src/.objects
    before chain.o(line 72) adding 
    
        bfgs.o dynmat.o instanton.o lbfgs.o sd.o cg.o dimer.o bbm.o \
        fire.o lanczos.o neb.o qm.o opt.o \
        
    **be careful there is no space after \\**
    
6. load icc and impi

        $ module load icc/16.0
        $ module load impi/2016
    
7. setting MKLROOT variable

    check the path of ifort and set MKLROOT
        
        $ which ifort
        /lustre/spack/tools/linux-centos7-x86_64/intel-16.0.4/intel-parallel-studio-cluster.2016.4-ybjjq75tqpzgzjc4drolyijzm45g5qul/compilers_and_libraries_2016.4.258/linux/bin/intel64/ifort
        $ export MKLROOT=/lustre/spack/tools/linux-centos7-x86_64/intel-16.0.4/intel-parallel-studio-cluster.2016.4-ybjjq75tqpzgzjc4drolyijzm45g5qul/compilers_and_libraries_2016.4.258/linux/mkl
        
8. Use arch/makefile.include.linux_intel as a template

        $ cp arch/makefile.include.linux_intel makefile.include
    
9. clean the files compiled before
    
        $ make veryclean

10. compile

        $ make
        
the binary files are in bin now including vasp_std vasp_gam vasp_ncl

Reference
======
* "VASP 5.4.1+VTST编译安装" <http://hmli.ustc.edu.cn/doc/app/vasp.5.4.1-vtst.htm>
* "VTST installation" <http://theory.cm.utexas.edu/vtsttools/installation.html>
    

    
    
    

