+++
date = "2015-10-28T21:24:03+08:00"
draft = false
title = "Data backup via rsync and cron"
+++

This short tutorial aims to explain how to use rsyn and cron to backup you data.

Rsync is a fast and efficient file copying tool. It can copy locally, to/from another host over any remote shell, or to/from a remote rsync daemon. It offers a large number of options that control every aspect of its behavior and permit very flexible specification of the set of files to be copied.

```Cron``` comes from the word chronos, the Greek word for time. Cron is a utility that can help with automating certain tasks in Linux. For example if you would like to create backups of certain files or directories each night while you are sleeping, you can use Cron to automate this.

Cron stores it’s entries in the ```crontab``` (cron table) file. To edit a users crontab entry, simply log into your system for that particular user and type ```crontab -e```. The default editor for the "crontab -e" command is vi. If you are not familiar with VI you can change the default editor by running the following command 
	export VISUAL=editor
	
Of course you must replace editor with your favorite text editor (nano, pico, joe etc). Before we start you may like to check those two links I feel useful about rsync and cron:

* http://everythinglinux.org/rsync/
* http://kvz.io/blog/2007/07/29/schedule-tasks-on-linux-using-crontab/

This document is not a tutorial about those two beautiful tools, so I strongly recommend the reader to check the ma pages. The idea is to execute periodically a script that will backup (copy) your data from π to a local disk.

My script, wich I call runbkp looks like this:

	#!/bin/bash
	TMPBKP=/path/to/your/home/tmp/bkp.log
	/path/to/your/home/script/runbackup.sh > $TMPBKP
	cat $TMPBKP | mailx -s "`date '+BKP ON %m/%d/%y AT %H:%M:%S'`" youremail@whatever.xxx
	
The scrip above just execute the backup script runbackup.sh bellow and email the output. In other word, cron will periodically execute runbkp and each time you will get the output on your mailbox. Rsync is actually call by runbackup.sh wich looks like this:

	#!/bin/bash
	
	DEST1=/media/usb1/pi
	SRC1=yourpilogin@202.120.58.230:
	LOG1=/path/to/your/home/tmp/pi-bkp.log
	
	ARG="--verbose --human-readable --executability --progress --stats --recursive --times --perms --links --delete --rsh=/usr/bin/ssh"
	
	echo " "
	date '+BKP STARTED ON %m/%d/%y AT %H:%M:%S'
	echo " "
	
	if grep -q usb1 /proc/mounts
	then
	
	 echo "usb1 is mounted: Proceeding ..."
	 echo " "
	
	 echo -n "`basename $DEST1` ... "
	 /usr/bin/rsync $ARG $SRC1 $DEST1 >& $LOG1
	 if [ $? -eq 0 ]
	 then
	 echo "OK"
	 else
	 echo "Error"
	 fi
	
	else
	 echo " "
	 echo "usb1 not mounted. Exiting."
	 echo " "
	 exit 1
	fi
	
	echo " "
	date '+BKP ENDED ON %m/%d/%y AT %H:%M:%S'
	echo " "

Suppose you have a disk mounted on /media/usb1, this script will request rsync to ssh to π and backup your data to /media/usb1/pi. Using those rsync arguments the backup is like an image of your current folder. Again, carefully read rsync documentation in order to understand them. The script also output the starting and ending date, as well as if the commands failed or not. This is the information the is emailed to you. The rsync output is redirected to a specific file since it can be quite long. Now you can try to execute first unbackup.sh and then runbkp. If everything goes well you can creat a cron table via the crontab command. Just type crontab -e to creat or edit it. In order to automatically backup your file you can add the following lines and save.

	SHELL=/bin/bash
	PATH=/sbin:/bin:/usr/sbin:/usr/bin:/path/to/your/home/script
	# run-parts
	0 11,21 * * * /path/to/your/home/script/runbkp

This will execute the runbkp everyday at 11am and 9pm.
