+++
date = "2018-07-19T09:43:41+08:00"
draft = false
title = "Gromacs"
+++

Installing Spack
======

	$ cd
	$ git clone https://github.com/sjtuhpcc/spack.git
    $ cd spack
    
Add the following settings to ```~/.bashrc``` or ```~/.bash_profile```.

	# Spack package management
    if [ -d "$HOME/spack" ]; then
        export SPACK_ROOT=$HOME/spack
        source $SPACK_ROOT/share/spack/setup-env.sh
    fi	

Using gromacs 5.1.5 GPU version
======

Install gromacs:

    $ spack install gromacs+cuda+mpi@5.1.5 %gcc@5.4.0 ^openmpi+pmi+cuda schedulers=slurm ^cuda@8.0.61 

The following job (```gpu.slurm```) requests 2 GPU cards, with one CPU process managing one GPU card per node. Gromacs GPU node can be submitted via ```sbatch -N 1 gpu.slurm```.

    #!/bin/bash

    #SBATCH -J gromacs
    #SBATCH -p gpu
    #SBATCH --mail-type=end
    #SBATCH --mail-user=YOU@EMAIL.COM
    #SBATCH -o %j.out
    #SBATCH -e %j.err
    #SBATCH --ntasks-per-node=2
    #SBATCH --gres=gpu:2
    #SBATCH --exclusive

    ulimit -s unlimited
    ulimit -l unlimited

    source /usr/share/Modules/init/bash

    # Spack
    if [ -d "$HOME/spack" ]; then
        export SPACK_ROOT=$HOME/spack
        source $SPACK_ROOT/share/spack/setup-env.sh
    fi

    source <(spack module tcl loads gromacs+cuda %gcc@5.4.0 )

    export OMP_NUM_THREADS=8

    INPUT=caseC_adh_cubic_pme_verlet.tpr

    srun --mpi=pmi2 gmx_mpi mdrun -s $INPUT -deffnm $INPUT -maxh 0.50 -resethway -noconfout -nsteps 10000
    
Using gromacs 5.1.5 CPU version
======

Install gromacs:

    $ spack install gromacs~cuda+mpi@5.1.5 %gcc@5.4.0 ^openmpi+pmi schedulers=slurm 

The following job (```cpu.slurm```) requests all 16 CPU cores per node, which can be submitted via ```sbatch -N 1 cpu.slurm```.

    #!/bin/bash

    #SBATCH -J gromacs
    #SBATCH -p cpu
    #SBATCH --mail-type=end
    #SBATCH --mail-user=YOU@EMAIL.COM
    #SBATCH -o %j.out
    #SBATCH -e %j.err
    #SBATCH --ntasks-per-node=16

    ulimit -s unlimited
    ulimit -l unlimited

    source /usr/share/Modules/init/bash

    # Spack
    if [ -d "$HOME/spack" ]; then
        export SPACK_ROOT=$HOME/spack
        source $SPACK_ROOT/share/spack/setup-env.sh
    fi

    source <(spack module tcl loads gromacs~cuda %gcc@5.4.0 )

    INPUT=caseC_adh_cubic_pme_verlet.tpr

    srun --mpi=pmi2 gmx_mpi mdrun -s $INPUT -deffnm $INPUT -maxh 0.50 -resethway -noconfout -nsteps 10000
