+++
title = "Lammps"
date =  2019-04-10T10:03:31+08:00
+++

Installing Spack
======

	$ cd
	$ git clone https://github.com/sjtuhpcc/spack.git
    $ cd spack
    
Add the following settings to ```~/.bashrc``` or ```~/.bash_profile```.

	# Spack package management
    if [ -d "$HOME/spack" ]; then
        export SPACK_ROOT=$HOME/spack
        source $SPACK_ROOT/share/spack/setup-env.sh
    fi	

Using lammps
======

Install lammps:

    $ spack install lammps@20180822+class2+colloid+compress+coreshell+dipole+granular+kspace +lib+manybody+mc+meam+misc+molecule+mpi+mpiio+peri+poems+python+qeq+reax+replica+rigid+shock+snap+srd+user-atc+user-h5md+user-lb+user-misc+user-netcdf+user-omp %gcc@5.4.0 ^openmpi+pmi fabrics=verbs schedulers=slurm ^fftw~mpi ^cmake@3.9.6

The following job (```cpu.slurm```) requests all 16 CPU cores per node, which can be submitted via ```sbatch -N 1 cpu.slurm```.

    #!/bin/bash

    #SBATCH -J lammps
    #SBATCH -p cpu
    #SBATCH --mail-type=end
    #SBATCH --mail-user=YOU@EMAIL.COM
    #SBATCH -o %j.out
    #SBATCH -e %j.err
    #SBATCH -N1
    #SBATCH --exclusive

    ulimit -s unlimited
    ulimit -l unlimited

    source <( spack module tcl loads --dependencies lammps %gcc@5.4.0 )

    srun --mpi=pmi2 lmp