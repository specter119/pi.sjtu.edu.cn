+++
date = "2015-11-15T22:56:51+08:00"
draft = false
title = "Environment Modules"
weight = 20
+++

Environment modules can assist you to use prebuilt software packages on Pi. Each environment module is a set of environment settings that can be applied and unapplied on the fly. User can write their own modules.

This document introduce basic usage of environment modules along with available software modules on Pi.

Modules Commands
======

| Command                   | Function                                |
|---------------------------|-----------------------------------------|
| `module use [PATH]`       | Add files under [PATH] into module list |
| `module avail`            | List all modules                        |
| `module load [MODULE]`    | Load [MODULE]                           |
| `module unload [MODULE]`  | Unload [MODULE]                         |
| `module purge`            | Unload all modules                      |
| `module whatis  [MODULE]` | Show basic info about [MODULE]          |
| `module info [MODULE]`    | Show detailed info about [MODULE]       |
| `module display [MODULE]` | Display info about [MODULE]             |
| `module load [MODULE]`    | Load [MODULE]                           |
| `module help`             | Get help                                |

`module use [PATH]`: Import module files
------

`module use` will import all available module files under [PATH] which will be available in module commands. Autally, `[PATH]` is added to the beginning of `MODULEPATH` environment variable.

`module avail`: List available modules
------

    $ mdoule avail
    ------------------------------------------------------------------------ /lustre/usr/modulefiles -------------------------------------------------------------------------

`module purge/load/unload/list`: A complete module workflow
------

It is a good practice to uanload all loaded modules before starting a new job.

    $ mdoule purge
    
Multiple modules can be loaded or unloaded at a time.

    $ mdoule load gcc openmpi
    $ module unload gcc openmpi

You can check loaded modules at anytime of your work.

    $ module list

Smart modules for SLURM
======

On SLURM, we applied the following rules to derive the most appropriate modules. 

1. *Compiler*: If `gcc` or `icc` is loaded, load the modules compiled against the corresponding compiler. Or the default compiler, gcc, will be loaded when necessary.
2. *MPI Library*: If one of those libraries (`openmpi`, `impi`, `mvapich2`, `mpich`) is loaded, load the modules compiled against the corresponding MPI. Or the default MPI lib, openmpi, will be loaded when necessary.
3. *Module veriosn*: A default version is set for each module, which will be loaded if no version number is specified.

On SLURM, the following clauses have the same effect as the one does above.

    $ module load gcc/5.4 openmpi/3.1
    
Or, version number can be neglected if you prefer the latest stable version. 

    $ module load gcc openmpi

Software Modules on Pi
======

Pi has many prebuilt software modules and the number keeps growing. You are welcome to tell us the popular software in your research fields. As being charged little or even zero, opensource software will have higher priority to install.

Software on Pi can be categorized into compilers and platforms, MPI libs, Math libs, FD tools, bioinfo tools, and so on.

Compilers and Platforms
------

| Module Name      | Description                 | Available Versions  | Default Version | Notes |
|------------------|-----------------------------|---------------------|-----------------|-------|
| [gcc][gcc]       | GNU compile collection      | 4.6 4.7 4.8 4.9 5.1 | 4.9             |       |
| [icc][icc]       | Intel compiler suite        | 14.0 15.0           | 15.0            |       |
| [pgi][pgi]       | PGI compiler                | 15.7                | 15.7            |       |
| [cuda][cuda]     | NVIDIA CUDA SDK             | 6.5 7.5             | 7.5             |       |
| [jdk][jdk]       | Java development kit        | 1.6  1.7 1.8        | 1.8             |       |

MPI Libraries
------

| Module Name          | Description | Available Versions | Default Version | Notes |
|----------------------|-------------|--------------------|-----------------|-------|
| [openmpi][openmpi]   | OpenMPI     | 1.6 1.10           | 1.10            |       |
| [mvapich2][mvapich2] | MVAPICH2    | 2.0 2.1            | 2.1             |       |
| [impi][impi]         | Intel MPI   | 4.1 5.0            | 5.0             |       |

Math Libraries
------

| Module Name          | Description                         | Available Versions | Default Version | Notes                                                             |
|----------------------|-------------------------------------|--------------------|-----------------|-------------------------------------------------------------------|
| [mkl][mkl]           | Intel Math Kernel Librarires        | 11.1 11.2          | 11.2            | Containing FFTW, BLAS, LAPACK implementaitons                     |

Computer Vision and Deep Learning
------

| Module Name      | Description | Available Versions | Default Version | GPU enabled | Notes |
|------------------|-------------|--------------------|-----------------|-------------|-------|
| [cudnn][cudnn]   |             | 1.0 2.0 3.0        | 3.0             | Yes         |       |

Tools for Building and Tuning Software
------
| Module Name | Description | Available Versions | Default Version | Notes |
|-------------|-------------|--------------------|-----------------|-------|
| [maven][maven]       |             | 3.3                | 3.3             |       |
| [bazel][bazel]       |             | 0.1                | 0.1             |       |
| [vtune][vtune]          | Intel VTune                | 5.1                | 5.1             |       |

Reference
=====
* "Environment Modules Project" <http://modules.sourceforge.net/>
* "Modules Software Environment on NERSC" <https://www.nersc.gov/users/software/nersc-user-environment/modules/>

[modules]: http://modules.sourceforge.net 
[gcc]: https://gcc.gnu.org
[icc]: https://software.intel.com/en-us/intel-compilers
[pgi]: http://www.pgroup.com
[cuda]: http://www.nvidia.com/object/cuda_home_new.html
[python]: https://www.python.org
[R]: https://www.r-project.org
[perl]: https://www.perl.org
[jdk]: http://openjdk.java.net
[scotch]: http://scotch.gforge.inria.fr
[openmpi]: http://www.open-mpi.org
[mvapich2]: http://mvapich.cse.ohio-state.edu
[mpich]: https://www.mpich.org
[impi]: https://software.intel.com/en-us/intel-mpi-library
[mkl]: https://software.intel.com/en-us/intel-mkl
[gsl]: http://www.gnu.org/software/gsl/
[fftw2]: http://www.fftw.org
[fftw3]: http://www.fftw.org
[atlas]: http://math-atlas.sourceforge.net
[openblas]:  http://www.openblas.net
[lapack]: http://www.netlib.org/lapack/
[octave]: https://www.gnu.org/software/octave/
[scilab]: http://www.scilab.org 
[eigen]: http://eigen.tuxfamily.org
[gmp]: https://gmplib.org
[mpfr]: http://www.mpfr.org
[mpc]: http://mpc.multiprecision.org
[cgal]: http://www.cgal.org
[abyss]: http://www.bcgsc.ca/platform/bioinfo/software/abyss
[map]: http://maq.sourceforge.net
[bcftools]: https://samtools.github.io/bcftools/bcftools.html
[bowtie]: http://bowtie-bio.sourceforge.net/index.shtml
[bwa]: http://bio-bwa.sourceforge.net
[gatk]: https://www.broadinstitute.org/gatk
[samtools]: https://samtools.github.io
[smufin]: http://cg.bsc.es/smufin
[acend]: http://ascend4.org
[htslib]: http://www.htslib.org
[soapdenovo]: http://soap.genomics.org.cn/soapdenovo.html
[gromacs]: http://www.gromacs.org
[cudnn]: https://developer.nvidia.com/cudnn
[opencv]: http://opencv.org
[ffmpeg]: https://www.ffmpeg.org
[caffe]: http://caffe.berkeleyvision.org
[boost]: http://www.boost.org
[sparsehash]: https://github.com/sparsehash/sparsehash
[cityhash]: https://github.com/google/cityhash
[netcdf]: http://www.unidata.ucar.edu/netcdf
[hdf5]: https://www.hdfgroup.org/HDF5/
[phdf5]: https://www.hdfgroup.org/HDF5/
[pnetcdf]: https://trac.mcs.anl.gov/projects/parallel-netcdf
[szip]: https://www.hdfgroup.org/doc_resource/SZIP/
[ipp]: https://software.intel.com/en-us/intel-ipp
[tbb]: https://www.threadingbuildingblocks.org
[maven]: https://maven.apache.org
[bazel]: http://bazel.io
[vtune]: https://software.intel.com/en-us/intel-vtune-amplifier-xe
[mdtest]: http://sourceforge.net/projects/mdtest/
[openfoam]: http://www.openfoam.com
[htslib]: http://www.htslib.org

