+++
title = "Amber"
date =  2018-11-28T15:35:41+08:00
+++

Install Amber14 MPI version
======

Download Amber14 on http://ambermd.org/Amber14-get.htmlTools and AmberTools14 on http://ambermd.org/AmberTools14-get.html, and then upload them to the login load of PI.

Then load basic modules and extract the tar.

	$ module purge
	$ module load gcc/5.4 openmpi/3.1
	$ tar -xvjf AmberTools14.tar.bz2                 
	$ tar -xvjf Amber14.tar.bz2

Configure Amber14.

    $ cd amber14
    $ export AMBERHOME=$HOME/amber14  
    $ ./configure -mpi -noX11 gnu

Install Amber14.

	$ make install

To use Amber 14, source amber.sh.

	$ source amber.sh


Install Amber14 GPU version
======

Download Amber14 on http://ambermd.org/Amber14-get.htmlTools and AmberTools14 on http://ambermd.org/AmberTools14-get.html, and then upload them to the login load of PI.

Then load basic modules and extract the tar.

	$ module purge
	$ module load gcc/4.8 openmpi/3.1 cuda/6.5
	$ tar -xvjf AmberTools14.tar.bz2                 
	$ tar -xvjf Amber14.tar.bz2

Configure Amber14.

    $ cd amber14
    $ export AMBERHOME=$HOME/amber14  
    $ ./configure -cuda -mpi -noX11 gnu

Install Amber14.

	$ make install

To use Amber 14, source amber.sh.

	$ source amber.sh

Reference
======
* "Amber 14 Reference Manual Chapter 2" <http://ambermd.org/doc12/Amber14.pdf>
