+++
date = "2015-10-28T21:21:10+08:00"
draft = false
title = "HTTP/HTTPS Proxy on Pi"
+++

Set the HTTP proxy in the command line. Both HTTP and HTTPS are required. You can write the settings to ```~/.bashrc```.

        $ export http_proxy=http://proxy.pi.sjtu.edu.cn:3004/
        $ export https_proxy=http://proxy.pi.sjtu.edu.cn:3004/

Try to access a HTTP web:

        $ curl -v http://www.sjtu.edu.cn

Download via http:

        $ wget http://net.sjtu.edu.cn/images/n1.png -O sample.png

Try to access a HTTPS site:

        $ curl -v https://bootstrap.pypa.io

Download via https:

        $ wget --no-check-certificate https://bootstrap.pypa.io/ez_setup.py
