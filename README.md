Hugo
======

Clone `pi.sjtu.edu.cn` git repo.

    $ git clone git@git.hpc.sjtu.edu.cn:docs/pi.sjtu.edu.cn.git  

Install theme named `sjtuhpc-learn`.

    $ cd pi.sjtu.edu.cn && mkdir themes; cd themes; rmdir sjtuhpc-learn
    $ git clone https://github.com/sjtuhpcc/hugo-theme-learn.git sjtuhpc-learn

Add a new page when necessary.

	$ cd pi.sjtu.edu.cn
	$ hugo new doc/a-new-page.md

Run Hugo locally and preview the site in your browser via http://localhost:1313 .

	$ hugo server --buildDrafts

Push pages to the web server.

	$ make update
