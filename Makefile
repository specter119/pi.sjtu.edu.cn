.PHONY : all git update dropbox server

WGET=wget --recursive --no-clobber --page-requisites --html-extension --convert-links --restrict-file-names=windows --no-parent -X "/ganglia"

all : git update

git :
	git pull
	git push

server :
	-@rm -rf public
	-@hugo server --buildDrafts
	-@cp -f static/index.html public

update :
	-@rm -rf public
	hugo
	scp -r -P 3022 public/* hpc@pi.sjtu.edu.cn://home/www/pi.sjtu.edu.cn/
	-@rm -rf public/

dropbox :
	-@rm -rf pi.sjtu.edu.cn
	-@$(WGET) --domains pi.sjtu.edu.cn pi.sjtu.edu.cn/log
	-@$(WGET) --domains pi.sjtu.edu.cn pi.sjtu.edu.cn/admin
	-@$(WGET) --domains pi.sjtu.edu.cn pi.sjtu.edu.cn/doc
	-@cp -f static/index.html public
	scp -r pi.sjtu.edu.cn/ ~/Dropbox/HPC-Staff/pi.sjtu.edu.cn/
	-@rm -rf pi.sjtu.edu.cn/
