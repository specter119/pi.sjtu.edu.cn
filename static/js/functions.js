function calRunTime() {
	var start	= new Date(2013, 10, 15, 18, 50, 00); // Y, M, D, h, m, s // month starts from 0
	var current	= new Date();
	var len		= current.getTime() - start.getTime();  //ms
	// document.writeln(start.toString());

	var day		= Math.floor(len/86400000); // 24 * 3600 * 1000 ms
		len		%= 86400000;
	var hour	= Math.floor(len/3600000); // 3600 * 1000 ms
		len		%= 3600000;
	var minute	= Math.floor(len/60000); // 60 * 1000 ms
		len		%= 60000;
	var second	= Math.floor(len/1000);

	var output = day + " day(s) " + hour + " hour(s) " + minute + " minute(s) " + second + " second(s)";
	document.getElementById('total_runtime').innerText = output;
}

function updateRunTime() {
	setInterval('calRunTime()', 1000);
}